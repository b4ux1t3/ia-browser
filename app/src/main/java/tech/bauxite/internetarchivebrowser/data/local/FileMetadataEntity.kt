package tech.bauxite.internetarchivebrowser.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
//    foreignKeys = [
//        ForeignKey(
//            entity = ItemMetadataEntity::class,
//            parentColumns = ["id"],
//            childColumns = ["item"],
//            onDelete = ForeignKey.CASCADE
//        )
//    ]
)
data class FileMetadataEntity(
    val name: String,
    val source: String,
    val sha1: String,
    val format: String,
    val size: Int,
    val original: String?,
    @PrimaryKey val id: Int? = null,
//    @ColumnInfo(index = true)
    var item: String?
)
