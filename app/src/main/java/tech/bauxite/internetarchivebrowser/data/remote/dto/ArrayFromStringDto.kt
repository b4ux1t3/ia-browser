package tech.bauxite.internetarchivebrowser.data.remote.dto

data class ArrayFromStringDto(val strings: List<String>)