package tech.bauxite.internetarchivebrowser.data.remote.dto

data class StringFromArrayDto(private val _string: String?) {
    val string: String
    get() = _string?: ""

    override fun toString(): String {
        return _string ?: ""
    }
}