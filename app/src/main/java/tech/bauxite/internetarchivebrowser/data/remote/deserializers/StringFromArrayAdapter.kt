package tech.bauxite.internetarchivebrowser.data.remote.deserializers

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import tech.bauxite.internetarchivebrowser.data.remote.dto.StringFromArrayDto

class StringFromArrayAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader, delegate: JsonAdapter<List<String>>): StringFromArrayDto {
        return when (jsonReader.peek()) {
            JsonReader.Token.BEGIN_ARRAY -> {
                val array = delegate.fromJson(jsonReader) ?: emptyList()
                StringFromArrayDto(array.joinToString("\n"))
            }
            else -> {
                StringFromArrayDto(jsonReader.nextString())
            }
        }
    }
}