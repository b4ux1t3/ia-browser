package tech.bauxite.internetarchivebrowser.data.local

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

data class ItemAndFiles(
    @Embedded val item: ItemMetadataEntity,
    @Relation(
        parentColumn = "identifier",
        entityColumn = "item",
        entity = FileMetadataEntity::class
    )
    val files: List<FileMetadataEntity>
)
