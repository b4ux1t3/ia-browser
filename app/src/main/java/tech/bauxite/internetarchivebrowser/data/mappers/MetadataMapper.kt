package tech.bauxite.internetarchivebrowser.data.mappers

import tech.bauxite.internetarchivebrowser.data.local.FileMetadataEntity
import tech.bauxite.internetarchivebrowser.data.local.ItemMetadataEntity
import tech.bauxite.internetarchivebrowser.data.remote.dto.ItemMetadataDto
import tech.bauxite.internetarchivebrowser.domain.model.ItemMetadata
import java.lang.NumberFormatException
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun ItemMetadataDto.toDomain(identifier: String): ItemMetadata {
    var lastUpdated: LocalDateTime? = null

    this.lastUpdated?.let {
        lastUpdated = LocalDateTime
            .ofInstant(
                Instant.ofEpochSecond(it),
                ZoneId.systemDefault()
            )
    }

    var addedDate: LocalDateTime? = null
    var publicDate: LocalDateTime? = null

    val pattern = "yyyy-MM-dd HH:mm:ss"
    val formatter = DateTimeFormatter.ofPattern(pattern, Locale.getDefault())
    this.metadata?.let {
        it.addedDate?.let { date ->
            val wholeDate = date.split(".")[0] // Some items have fractional times. We don't want that.
            addedDate = LocalDateTime.parse(wholeDate, formatter)

        }
        it.publicDate?.let { date ->
            val wholeDate = date.split(".")[0]
            publicDate = LocalDateTime.parse(wholeDate, formatter)
        }
    }

    val imageCount = try {
        metadata?.imageCount?.toInt()
    } catch(ex: NumberFormatException) {
        null
    }
    return ItemMetadata(
        serverOne = serverOne,
        serverTwo = serverTwo,
        directory = directory,
        files = files.map { it.toDomain() },
        identifier = metadata?.identifier ?: identifier,
        addedDate = addedDate,
        publicDate = publicDate,
        description = metadata?.description.toString(),
        coverLeaf = metadata?.coverLeaf,
        creator = metadata?.creator?.strings ?: emptyList(),
        curation = metadata?.curation,
        imageCount = imageCount,
        language = metadata?.language,
        lastUpdated = lastUpdated,
        licenseUrl = metadata?.licenseUrl,
        mediaType = metadata?.mediaType,
        subject = metadata?.subjects?.strings ?: emptyList(),
        title = metadata?.title,
        uploader = metadata?.uploader
    )
}

fun ItemMetadataDto.ItemFileMetadataDto.toDomain(): ItemMetadata.ItemFileMetadata = ItemMetadata.ItemFileMetadata(
    name = name ?: "",
    format = format ?: "",
    original = original,
    sha1 = sha1 ?: "",
    size = size ?: 0,
    source = source ?: ""
)

fun ItemMetadata.toEntity(): ItemMetadataEntity = ItemMetadataEntity(
    serverOne = serverOne,
    serverTwo = serverTwo,
    directory = directory,
    identifier = identifier,
    addedDate = addedDate.toString(),
    publicDate = publicDate.toString(),
    description = description,
    coverLeaf = coverLeaf,
    curation = curation,
    imageCount = imageCount,
    language = language,
    licenseUrl = licenseUrl,
    mediaType = mediaType,
    title = title,
    subject = subject.joinToString(separator = "|"),
    uploader = uploader,
    creator = creator.joinToString(separator = "|"),
    lastUpdated = lastUpdated.toString()
)

fun ItemMetadata.ItemFileMetadata.toEntity(itemMetadataEntity: ItemMetadataEntity): FileMetadataEntity = FileMetadataEntity (
    name = name,
    format = format,
    original = original,
    sha1 = sha1,
    size = size,
    source = source,
    item = itemMetadataEntity.identifier
)

fun ItemMetadataEntity.toDomain(files: List<FileMetadataEntity>): ItemMetadata = ItemMetadata(
    serverOne = serverOne,
    serverTwo = serverTwo,
    directory = directory,
    identifier = identifier,
    files = files.map { it.toDomain() },
    uploader = uploader,
    title = title,
    subject = subject?.split("|") ?: emptyList(),
    mediaType = mediaType,
    licenseUrl = licenseUrl,
    lastUpdated = LocalDateTime.parse(lastUpdated),
    language = language,
    imageCount = imageCount,
    curation = curation,
    creator = creator?.split("|") ?: emptyList(),
    coverLeaf = coverLeaf,
    description = description ?: "",
    publicDate = LocalDateTime.parse(publicDate),
    addedDate = LocalDateTime.parse(addedDate)
)

fun FileMetadataEntity.toDomain(): ItemMetadata.ItemFileMetadata = ItemMetadata.ItemFileMetadata (
    name = name,
    format = format,
    original = original,
    sha1 = sha1,
    size = size,
    source = source,
)