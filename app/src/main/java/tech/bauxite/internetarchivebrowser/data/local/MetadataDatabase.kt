package tech.bauxite.internetarchivebrowser.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database (
    entities = [
        ItemMetadataEntity::class,
        FileMetadataEntity::class
    ],
    version = 1
    )
abstract class MetadataDatabase: RoomDatabase() {
    abstract val dao: MetadataDao
}