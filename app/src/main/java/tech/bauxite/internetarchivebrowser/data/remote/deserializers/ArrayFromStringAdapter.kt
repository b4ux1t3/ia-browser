package tech.bauxite.internetarchivebrowser.data.remote.deserializers

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import tech.bauxite.internetarchivebrowser.data.remote.dto.ArrayFromStringDto

class ArrayFromStringAdapter {
    @FromJson
    fun fromJson(jsonReader: JsonReader, delegate: JsonAdapter<List<String>>): ArrayFromStringDto {
        return when (jsonReader.peek()) {
            JsonReader.Token.BEGIN_ARRAY -> {
                ArrayFromStringDto(delegate.fromJson(jsonReader) ?: emptyList())
            }
            else -> {
                ArrayFromStringDto(jsonReader.nextString().split(";"))
            }
        }
    }
}