package tech.bauxite.internetarchivebrowser.data.mappers

import tech.bauxite.internetarchivebrowser.data.remote.dto.SearchQueryResponseDto
import tech.bauxite.internetarchivebrowser.domain.model.SearchResult

fun SearchQueryResponseDto.ResponseDto.DocListingDto.toDomain(): SearchResult = SearchResult(
    title = title,
    mediaType = mediaType,
    publicDate = publicDate,
    description = description?.toString() ?: "",
    identifier = identifier,
    backupLocation = backupLocation,
    downloads = downloads,
    format = format.strings,
    itemSize = itemSize,
    month = month,
)