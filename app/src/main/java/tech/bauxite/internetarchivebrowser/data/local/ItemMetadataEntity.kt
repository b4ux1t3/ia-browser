package tech.bauxite.internetarchivebrowser.data.local

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.time.LocalDateTime
import java.util.*

@Entity
data class ItemMetadataEntity(
    @PrimaryKey val identifier: String,
    val serverOne: String?,
    val serverTwo: String?,
    val directory: String?,
    val subject: String?,
    val creator: String?,
    val licenseUrl: String?,
    val uploader: String?,
    val addedDate: String?,
    val publicDate: String?,
    val curation: String?,
    val imageCount: Int?,
    val language: String?,
    val coverLeaf: String?,
    val mediaType: String?,
    val title: String?,
    val description: String?,
    val lastUpdated: String?,
)
