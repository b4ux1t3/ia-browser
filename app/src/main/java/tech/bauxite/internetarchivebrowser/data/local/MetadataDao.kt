package tech.bauxite.internetarchivebrowser.data.local

import androidx.room.*

@Dao
interface MetadataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMetadata(
        itemMetadataEntity: ItemMetadataEntity
    )

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFileMetadata(
        fileMetadataEntity: FileMetadataEntity
    )

    @Query(
        """
            SELECT *
            FROM itemmetadataentity
            WHERE LOWER(identifier) LIKE '%' || LOWER(:query) || '%' 
        """
    )
    suspend fun searchItems(query: String): List<ItemMetadataEntity>

    @Transaction
    @Query(
        """
            SELECT * 
            FROM itemmetadataentity
            WHERE identifier = :item
        """
    )
    suspend fun getItemWithFiles(item: String): List<ItemAndFiles>
}
