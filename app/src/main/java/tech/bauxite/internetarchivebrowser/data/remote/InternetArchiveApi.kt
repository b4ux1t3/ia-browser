package tech.bauxite.internetarchivebrowser.data.remote

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import tech.bauxite.internetarchivebrowser.data.remote.dto.ItemMetadataDto
import tech.bauxite.internetarchivebrowser.data.remote.dto.SearchQueryResponseDto
import tech.bauxite.internetarchivebrowser.domain.model.SearchResult

interface InternetArchiveApi {
    // GET: query the API
    @GET("/advancedsearch.php")
    suspend fun sendQuery(
        @Query("q") query: String,
        @Query("output") outputFormat: String = "json",
        @Query("rows") rows: Int = 50,
        @Query("page") page: Int = 1,
        @Query("fl[]=avg_rating") avg_rating: String = "avg_rating",
        @Query("fl[]=backup_location") backup_location: String = "backup_location",
        @Query("fl[]=btih") btih: String = "btih",
        @Query("fl[]=call_number") call_number: String = "call_number",
        @Query("fl[]=collection") collection: String = "collection",
        @Query("fl[]=contributor") contributor: String = "contributor",
        @Query("fl[]=coverage") coverage: String = "coverage",
        @Query("fl[]=creator") creator: String = "creator",
        @Query("fl[]=date") date: String = "date",
        @Query("fl[]=description") description: String = "description",
        @Query("fl[]=downloads") downloads: String = "downloads",
        @Query("fl[]=external-identifier") external_identifier: String = "external-identifier",
        @Query("fl[]=foldoutcount") foldoutcount: String = "foldoutcount",
        @Query("fl[]=format") format: String = "format",
        @Query("fl[]=genre") genre: String = "genre",
        @Query("fl[]=identifier") identifier: String = "identifier",
        @Query("fl[]=imagecount") imagecount: String = "imagecount",
        @Query("fl[]=indexflag") indexflag: String = "indexflag",
        @Query("fl[]=item_size") item_size: String = "item_size",
        @Query("fl[]=language") language: String = "language",
        @Query("fl[]=licenseurl") licenseurl: String = "licenseurl",
        @Query("fl[]=mediatype") mediatype: String = "mediatype",
        @Query("fl[]=members") members: String = "members",
        @Query("fl[]=month") month: String = "month",
        @Query("fl[]=name") name: String = "name",
        @Query("fl[]=noindex") noindex: String = "noindex",
        @Query("fl[]=num_reviews") num_reviews: String = "num_reviews",
        @Query("fl[]=oai_updatedate") oai_updatedate: String = "oai_updatedate",
        @Query("fl[]=publicdate") publicdate: String = "publicdate",
        @Query("fl[]=publisher") publisher: String = "publisher",
        @Query("fl[]=related-external-id") related_external_id: String = "related-external-id",
        @Query("fl[]=reviewdate") reviewdate: String = "reviewdate",
        @Query("fl[]=rights") rights: String = "rights",
        @Query("fl[]=scanningcentre") scanningcentre: String = "scanningcentre",
        @Query("fl[]=source") source: String = "source",
        @Query("fl[]=stripped_tags") stripped_tags: String = "stripped_tags",
        @Query("fl[]=subject") subject: String = "subject",
        @Query("fl[]=title") title: String = "title",
        @Query("fl[]=type") type: String = "type",
        @Query("fl[]=volume") volume: String = "volume",
        @Query("fl[]=week") week: String = "week",
        @Query("fl[]=year") year: String = "year",
    ): SearchQueryResponseDto

    // GET: metadata for a specific Item
    @GET("/metadata/{Identifier}")
    suspend fun getMetadata(
        @Path("Identifier") identifier: String
    ): ItemMetadataDto

    // GET: File associated with item.

    companion object {
        const val BASE_URL = "https://archive.org"
    }
}