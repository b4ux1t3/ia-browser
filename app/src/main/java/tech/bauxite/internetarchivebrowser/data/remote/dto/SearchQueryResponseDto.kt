package tech.bauxite.internetarchivebrowser.data.remote.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

data class SearchQueryResponseDto(
    @field:Json(name = "responseHeader") val responseHeader: ResponseHeaderDto?,
    @field:Json(name = "response") val response: ResponseDto?,

    ) {
    data class ResponseHeaderDto (
        @field:Json(name = "status") val status: Int?,
        @field:Json(name = "QTime") val qTime: Int?,
        @field:Json(name = "params") val queryParams: QueryParamsDto
    ) {
        data class QueryParamsDto(
            @field:Json(name = "query") val query: String?,
            @field:Json(name = "qin") val qin: String?,
            @field:Json(name = "fields") val fields: String?,
            @field:Json(name = "wt") val wt: String?,
            @field:Json(name = "rows") val rows: String?,
            @field:Json(name = "json.wrf") val jsonWrf: String?,
            @field:Json(name = "start") val start: Int?
        )
    }

    data class ResponseDto(
        @field:Json(name = "numFound") val numFound: Int?,
        @field:Json(name = "start") val start: Int?,
        val docs: List<DocListingDto>?
    ) {
        data class DocListingDto(
            @field:Json(name = "backup_location") val backupLocation: String? = null,
            val collection: ArrayFromStringDto,
            val description: StringFromArrayDto?,
            val downloads: Long? = null,
            val format: ArrayFromStringDto,
            val identifier: String,
            @field:Json(name = "indexflag") val indexFlag: List<String> = emptyList(),
            val itemSize: Long? = null,
            @field:Json(name = "mediatype") val mediaType: String? = null,
            val month: Int? = null,
            @field:Json(name = "publicdate") val publicDate: String? = null,
            val title: String? = null,
            val week: Int? = null,
        )
    }


}
