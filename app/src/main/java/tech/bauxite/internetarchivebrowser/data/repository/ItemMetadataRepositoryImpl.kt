package tech.bauxite.internetarchivebrowser.data.repository

import android.util.Log
import com.squareup.moshi.JsonDataException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okio.IOException
import retrofit2.HttpException
import tech.bauxite.internetarchivebrowser.data.local.MetadataDatabase
import tech.bauxite.internetarchivebrowser.data.mappers.toDomain
import tech.bauxite.internetarchivebrowser.data.mappers.toEntity
import tech.bauxite.internetarchivebrowser.data.remote.InternetArchiveApi
import tech.bauxite.internetarchivebrowser.data.remote.dto.SearchQueryResponseDto
import tech.bauxite.internetarchivebrowser.domain.model.ItemMetadata
import tech.bauxite.internetarchivebrowser.domain.model.SearchResult
import tech.bauxite.internetarchivebrowser.domain.repository.ItemMetadataRepository
import tech.bauxite.internetarchivebrowser.utilities.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ItemMetadataRepositoryImpl @Inject constructor(
    private val api: InternetArchiveApi,
    db: MetadataDatabase
): ItemMetadataRepository {

    private val dao = db.dao

    override suspend fun getItemMetadata(identifier: String): Flow<Resource<ItemMetadata>> {
        Log.d("getItemMetadata","Attempting to fetch metadata for identifier: $identifier")
        return flow {
            emit(Resource.Loading(true))

            val localMetadataEntities = dao.searchItems(identifier)

            if (localMetadataEntities.isNotEmpty()) {
                if (localMetadataEntities.size > 1) {
                    emit(Resource.Error("Found more than one item for this identifier."))
                    emit(Resource.Loading(false))
                    return@flow
                }
                val metadataEntity = localMetadataEntities.first()
                val itemsWithFile = metadataEntity.identifier.let {
                    dao.getItemWithFiles(it).map { itemAndFiles ->
                    itemAndFiles.item.toDomain(itemAndFiles.files)
                }
                }
                if (itemsWithFile.count() > 1) {
                    emit(Resource.Error("Found more than one item for this identifier."))
                    emit(Resource.Loading(false))
                    return@flow
                }
                val localMetadata = itemsWithFile.firstOrNull()
                if (localMetadata != null) {
                    Log.d("getItemMetadata","Found local metadata for $identifier")
                    emit(Resource.Success(localMetadata))
                    emit(Resource.Loading(false))
                    return@flow
                }
                emit(Resource.Error("An unknown error occurred."))
                emit(Resource.Loading(false))
            }

            // Now we've established that we have nothing in our local database. Time to go fetch stuff!
            try {
                Log.d("getItemMetadata","Trying remote source: $identifier")
                val result = getMetadata(identifier)
                insertMetadata(result)
                Log.d("getItemMetadata","Found remote metadata: $identifier")
                emit(Resource.Success(result))
                emit(Resource.Loading(false))
            } catch(e: IOException) {
                e.printStackTrace()
                emit(Resource.Error("An IO operation failed."))
            } catch(e: HttpException) {
                e.printStackTrace()
                emit(Resource.Error(
                    data = null,
                    errorMessage = "An HTTP error occurred:\n\t${e.message.toString()}"
                ))
            }
        }
    }

    override suspend fun getSearchResults(query: String): Resource<List<SearchResult>> {
        val results: Resource<SearchQueryResponseDto> = try {
            Resource.Success(api.sendQuery(query))
        } catch(e: IOException) {
            e.printStackTrace()
            Resource.Error("An IO operation failed.")
        } catch(e: HttpException) {
            e.printStackTrace()
            Resource.Error(
                data = null,
                errorMessage = "An HTTP error occurred:\n\t${e.message.toString()}"
            )
        } catch (e: JsonDataException) {
            e.printStackTrace()
            Resource.Error(
                data = null,
                errorMessage = "A parsing error occurred:\n\t${e.message.toString()}"
            )
        }
        return when (results) {
            is Resource.Success ->{
                Resource.Success(
                    results.data?.response?.docs?.map {
                        it.toDomain()
                    } ?: emptyList()
                )
            }

            is Resource.Error -> {
                Resource.Error(errorMessage = results.message ?: "An error occurred.")
            }

            else -> {
                Resource.Error(errorMessage = results.message ?: "An error occurred.")
            }
        }
    }

    private suspend fun getMetadata(identifier: String): ItemMetadata {
        val result = api.getMetadata(identifier)
        return result.toDomain(identifier)
    }

    private suspend fun insertMetadata(itemMetadata: ItemMetadata){
        val resultEntity = itemMetadata.toEntity()
        dao.insertMetadata(resultEntity)
        itemMetadata.files.forEach { file ->
            val entity = file.toEntity(resultEntity)
            dao.insertFileMetadata(entity)
        }
    }
}