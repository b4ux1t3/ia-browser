package tech.bauxite.internetarchivebrowser.data.remote.dto

import com.squareup.moshi.Json

data class ItemMetadataDto(
    @field:Json(name = "item_last_updated") val lastUpdated: Long?,
    @field:Json(name = "d1") val serverOne: String?,
    @field:Json(name = "d2") val serverTwo: String?,
    @field:Json(name = "dir") val directory: String?,
    @field:Json(name = "files") val files: List<ItemFileMetadataDto> = emptyList(),
    @field:Json(name = "metadata") val metadata: ItemMetadataDetailsDto?
) {
    data class ItemFileMetadataDto(
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "source") val source: String?,
        @field:Json(name = "sha1") val sha1: String?,
        @field:Json(name = "format") val format: String?,
        @field:Json(name = "size") val size: Int?,
        @field:Json(name = "original") val original: String?
    )

    data class ItemMetadataDetailsDto(
        @field:Json(name = "mediatype") val mediaType: String? = null,
        val title: String? = null,
        val description: StringFromArrayDto? = null,
        val subjects: ArrayFromStringDto,
        val creator: ArrayFromStringDto,
        @field:Json(name = "licenseurl") val licenseUrl: String? = null,
        val identifier: String? = null,
        val uploader: String? = null,
        @field:Json(name = "addeddate") val addedDate: String? = null,
        @field:Json(name = "publicdate")val publicDate: String? = null,
        val curation: String? = null,
        @field:Json(name = "imagecount") val imageCount: String? = null,
        @field:Json(name = "repub_state")val repubState: String? = null,
        val language: String? = null,
        @field:Json(name = "foldoutcount")val foldoutCount: String? = null,
        @field:Json(name = "coverleaf")val coverLeaf: String? = null,
    )
}
