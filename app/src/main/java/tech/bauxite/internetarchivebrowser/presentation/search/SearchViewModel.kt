package tech.bauxite.internetarchivebrowser.presentation.search

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tech.bauxite.internetarchivebrowser.domain.repository.ItemMetadataRepository
import tech.bauxite.internetarchivebrowser.utilities.Resource
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: ItemMetadataRepository
): ViewModel() {
    var state by mutableStateOf(SearchState())

    private var searchJob: Job? = null


    fun onEvent(event: SearchEvent) {
        when (event) {
            is SearchEvent.OnSearchQueryChange -> {
                state = state.copy(searchQuery = event.query)
                searchJob?.cancel()
                searchJob = viewModelScope.launch {
                    if (event.query.isNotBlank()) {
                        delay(500L)
                        doSearch()
                    }
                }
            }
            else -> Unit
        }
    }

    private fun doSearch(
        query: String = state.searchQuery
    ) {
        viewModelScope.launch {
            when (val result = repository.getSearchResults(query)) {
                is Resource.Success -> {
                    result.data?.let {
                        state = state.copy(
                            results = it
                        )
                    }
                }
                is Resource.Error -> Unit
                is Resource.Loading -> {
                    state = state.copy(isLoading = result.isLoading)
                }
            }
        }
    }
}

