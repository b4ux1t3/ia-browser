package tech.bauxite.internetarchivebrowser.presentation.items

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import tech.bauxite.internetarchivebrowser.R
import tech.bauxite.internetarchivebrowser.domain.model.ItemMetadata

@Composable
fun FileListItem(
    itemFileMetadata: ItemMetadata.ItemFileMetadata
){
    val painter = getIcon(itemFileMetadata.format)
    Row(modifier = Modifier.fillMaxWidth()){
        Text(
            text = itemFileMetadata.name,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(3f)
        )
        Icon(
            modifier = Modifier.weight(1f),
            painter = painter,
            contentDescription = itemFileMetadata.format
        )
    }
}

@Composable
private fun getIcon(format: String): Painter {
    return when (format) {
        "Text PDF" -> painterResource(R.drawable.ic_baseline_description_24)
        "Archive BitTorrent" -> painterResource(R.drawable.ic_baseline_cloud_download_24)
        "Animated GIF" -> painterResource(R.drawable.ic_baseline_image_24)

        else -> painterResource(R.drawable.ic_baseline_info_24)
    }
}