// We're "hoisting" (?) the identifier from here, so that it's accessible in our state.
@file:Suppress("UNUSED_PARAMETER")

package tech.bauxite.internetarchivebrowser.presentation.items

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination

@Composable
@Destination
fun ItemDetails(
    identifier: String,
    viewModel: ItemViewModel = hiltViewModel()
){
    val state = viewModel.state
    if (state.error == null && state.itemMetadata != null) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            item{
                Text(
                    text = state.itemMetadata.identifier,
                    fontWeight = FontWeight.Bold,
                    fontSize = MaterialTheme.typography.h4.fontSize,
                )
            }
            item{
                if (state.extendDescription) {
                    val scrollState = rememberScrollState()
                    Text(
                        text = state.itemMetadata.description,
                        modifier = Modifier
                            .fillMaxWidth()
                            .heightIn(20.dp, 500.dp)
                            .verticalScroll(scrollState),
                        softWrap = true,
                    )
                } else {
                    Text(
                        text = state.itemMetadata.description,
                        modifier = Modifier.fillMaxWidth(),
                        softWrap = true,
                        maxLines = 10,
                        overflow = TextOverflow.Ellipsis
                    )
                }
                Button(
                    onClick = { viewModel.toggleDescription() }
                ) {
                    Text(if (state.extendDescription) "Less" else "More")
                }
            }
            item {
                ExpandableListCard(
                    list = state.itemMetadata.files,
                    header = "Files",
                    modifier = Modifier.heightIn(20.dp, 500.dp))
            }
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(), contentAlignment = Center
    ) {
        if (state.isLoading) CircularProgressIndicator()
        else if (state.error != null) Text(text = state.error, color = MaterialTheme.colors.error)
    }


}