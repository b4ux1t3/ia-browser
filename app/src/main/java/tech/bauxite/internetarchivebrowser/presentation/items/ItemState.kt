package tech.bauxite.internetarchivebrowser.presentation.items

import tech.bauxite.internetarchivebrowser.domain.model.ItemMetadata

data class ItemState(
    val itemMetadata: ItemMetadata? = null,
    val error: String? = null,
    val isLoading: Boolean = false,
    val extendDescription: Boolean = false,
)
