package tech.bauxite.internetarchivebrowser.presentation.search

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import tech.bauxite.internetarchivebrowser.domain.model.SearchResult
import tech.bauxite.internetarchivebrowser.presentation.destinations.ItemDetailsDestination
import tech.bauxite.internetarchivebrowser.ui.theme.Shapes

@Composable
fun SearchResult(
    searchResult: SearchResult,
    modifier: Modifier = Modifier,
    navigator: DestinationsNavigator
) {
    Card(
        modifier = Modifier
            .padding(8.dp)
            .background(MaterialTheme.colors.surface)
            .shadow(
                shape = Shapes.small,
                ambientColor = Color.Gray,
                elevation = 8.dp,
            )
            .clickable {
                       navigator.navigate(ItemDetailsDestination(searchResult.identifier))
            },
        shape = Shapes.small,

    ) {
        Column(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    text = searchResult.title?: searchResult.identifier,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp,
                    color = MaterialTheme.colors.onBackground,
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    modifier = Modifier.weight(1f),
                )
                Spacer(modifier = Modifier.width(4.dp))
                Text(
                    text = searchResult.mediaType?: "Unknown type",
                    fontWeight = FontWeight.Light,
                    color = MaterialTheme.colors.onBackground
                )

            }
            Spacer(modifier = Modifier.width(16.dp))
            Text(
                text = searchResult.description?: "",
                fontWeight = FontWeight.Light,
                color = MaterialTheme.colors.onBackground,
                maxLines = 3,
                overflow = TextOverflow.Clip,
                modifier = Modifier.padding(16.dp)
            )
        }
    }
}