package tech.bauxite.internetarchivebrowser.presentation.search

import tech.bauxite.internetarchivebrowser.domain.model.SearchResult

data class SearchState(
    val results: List<SearchResult> = emptyList(),
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false,
    val searchQuery: String = ""
)
