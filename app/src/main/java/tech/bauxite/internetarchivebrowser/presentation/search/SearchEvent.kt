package tech.bauxite.internetarchivebrowser.presentation.search

sealed class SearchEvent {
    object Refresh: SearchEvent()
    data class OnSearchQueryChange(val query: String): SearchEvent()
}