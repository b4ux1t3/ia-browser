package tech.bauxite.internetarchivebrowser.presentation.items

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import tech.bauxite.internetarchivebrowser.domain.repository.ItemMetadataRepository
import tech.bauxite.internetarchivebrowser.utilities.Resource
import tech.bauxite.internetarchivebrowser.utilities.sanitizeText
import javax.inject.Inject

@HiltViewModel
class ItemViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val repository: ItemMetadataRepository
): ViewModel() {
    var state by mutableStateOf(ItemState())

    init {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val itemIdentifier = savedStateHandle.get<String>("identifier") ?: return@launch
            repository
                .getItemMetadata(itemIdentifier)
                .collect { result ->
                    when (result) {
                        is Resource.Success -> {
                            result.data?.let {
                                state = state.copy(
                                    itemMetadata = it.copy(description = sanitizeText(it.description)),
                                    error = null,
                                    isLoading = false
                                )
                            }
                        }
                        is Resource.Error -> {
                            state = state.copy(
                                itemMetadata = null,
                                error = result.message,
                                isLoading = false
                            )
                        }
                        is Resource.Loading -> {
                            state = state.copy(
                                isLoading = result.isLoading
                            )
                        }
                    }
                }
        }
    }

    fun toggleDescription() {
        state = state.copy(
            extendDescription = !state.extendDescription
        )
    }

}