package tech.bauxite.internetarchivebrowser.di

import android.app.Application
import androidx.room.Room
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import tech.bauxite.internetarchivebrowser.data.local.MetadataDatabase
import tech.bauxite.internetarchivebrowser.data.remote.InternetArchiveApi
import tech.bauxite.internetarchivebrowser.data.remote.InternetArchiveApi.Companion.BASE_URL
import tech.bauxite.internetarchivebrowser.data.remote.deserializers.ArrayFromStringAdapter
import tech.bauxite.internetarchivebrowser.data.remote.deserializers.StringFromArrayAdapter
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideArchiveApi(): InternetArchiveApi {
        val moshi = Moshi.Builder()
            .add(StringFromArrayAdapter())
            .add(ArrayFromStringAdapter())
            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create()
    }

    @Provides
    @Singleton
    fun provideMetadataDatabase(app: Application): MetadataDatabase {
        return Room.databaseBuilder(
            app,
            MetadataDatabase::class.java,
            "metadatadb.db"
        ).build()
    }
}