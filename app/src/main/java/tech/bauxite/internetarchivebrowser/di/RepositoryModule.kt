package tech.bauxite.internetarchivebrowser.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import tech.bauxite.internetarchivebrowser.data.repository.ItemMetadataRepositoryImpl
import tech.bauxite.internetarchivebrowser.domain.repository.ItemMetadataRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindMetadataRepository(metadataRepositoryImpl: ItemMetadataRepositoryImpl): ItemMetadataRepository
}