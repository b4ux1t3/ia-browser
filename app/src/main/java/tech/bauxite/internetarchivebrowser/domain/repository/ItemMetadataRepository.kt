package tech.bauxite.internetarchivebrowser.domain.repository

import kotlinx.coroutines.flow.Flow
import tech.bauxite.internetarchivebrowser.domain.model.ItemMetadata
import tech.bauxite.internetarchivebrowser.domain.model.SearchResult
import tech.bauxite.internetarchivebrowser.utilities.Resource

interface ItemMetadataRepository {

    suspend fun getItemMetadata(identifier: String): Flow<Resource<ItemMetadata>>
    suspend fun getSearchResults(query: String): Resource<List<SearchResult>>

}