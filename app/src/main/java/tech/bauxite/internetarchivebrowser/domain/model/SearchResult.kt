package tech.bauxite.internetarchivebrowser.domain.model

data class SearchResult(
    val backupLocation: String?,
    val description: String?,
    val downloads: Long?,
    val format: List<String> = emptyList(),
    val identifier: String,
    val itemSize: Long?,
    val mediaType: String?,
    val month: Int?,
    val publicDate: String?,
    val title: String?,
) {

}
