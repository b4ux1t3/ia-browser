package tech.bauxite.internetarchivebrowser.domain.model

import java.time.LocalDateTime

data class ItemMetadata(
    val lastUpdated: LocalDateTime?,
    val identifier: String,
    val serverOne: String?,
    val serverTwo: String?,
    val directory: String?,
    val files: List<ItemFileMetadata> = emptyList(),
    val mediaType: String?,
    val title: String?,
    val description: String,
    val subject: List<String> = emptyList(),
    val creator: List<String> = emptyList(),
    val licenseUrl: String?,
    val uploader: String?,
    val addedDate: LocalDateTime?,
    val publicDate: LocalDateTime?,
    val curation: String?,
    val imageCount: Int?,
    val language: String?,
    val coverLeaf: String?,
) {
    data class ItemFileMetadata(
        val name: String,
        val source: String,
        val sha1: String,
        val format: String,
        val size: Int,
        val original: String? = null
    ) {
        val isOriginal = original == null
        val isDerivative = !isOriginal
    }
}