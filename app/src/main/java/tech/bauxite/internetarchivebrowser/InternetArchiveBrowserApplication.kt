package tech.bauxite.internetarchivebrowser

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InternetArchiveBrowserApplication: Application() {
}