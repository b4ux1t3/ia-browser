package tech.bauxite.internetarchivebrowser.utilities

import java.security.InvalidParameterException

private data class Replacement(val result: String, val replaced: Boolean = true, val attributes: String? = null)

private fun generateTagPairRegex(tagName: String): Regex {
    if (tagName.contains(" ")) throw InvalidParameterException("No spaces in HTML tags!\nTag name: $tagName")
    return Regex("<\\s*$tagName([^<>]*)/?>([^<]*)(<\\s*/$tagName\\s*>)?")
}

private fun generateSelfClosingTagRegex(tagName: String): Regex {
    if (tagName.contains(" ")) throw InvalidParameterException("No spaces in HTML tags!\nTag name: $tagName")
    return Regex("<\\s*$tagName([^<>]*)/\\s*>")
}

//Attributes
val attributeRegex: Regex = Regex("\\s+([^=\\s<>]+)(=\"?[^\">]*\"?)?")

private fun replaceFirstInstance(inputText: String, tagRegex: Regex, replacementText: String? = null): Replacement{
    return when (val match = tagRegex.find(inputText)) {
        null -> Replacement(inputText, false)
        else -> {
            val attributeString = match.groups[1]?.value
            val innerText = replacementText ?: (match.groups[2]?.value ?: "")
            Replacement(
                result = inputText.replaceFirst(tagRegex, innerText),
                attributes = attributeString
            )
        }
    }
}

private fun stripFirstInstance(inputText: String, tagRegex: Regex): Replacement =
    replaceFirstInstance(inputText, tagRegex, "")

private fun replaceAllInstances(inputText: String, tagRegex: Regex, replacementText: String? = null): String {
    var done = false
    var temp: String = inputText
    while (!done) {
        val replacement = replaceFirstInstance(temp, tagRegex, replacementText)
        temp = replacement.result
        done = !replacement.replaced
    }

    return temp
}

private fun stripAllInstances(inputText: String, tagRegex: Regex): String {
    var done = false
    var temp: String = inputText
    while (!done) {
        val replacement = stripFirstInstance(temp, tagRegex)
        temp = replacement.result
        done = !replacement.replaced
    }

    return temp
}

fun replaceTagName(inputText: String, tagName: String, replacement: String? = null): String = replaceAllInstances(
    inputText = inputText,
    tagRegex = generateTagPairRegex(tagName),
    replacementText = replacement
)

fun stripTagName(inputText: String, tagName: String): String = stripAllInstances(
    inputText = inputText,
    tagRegex = generateTagPairRegex(tagName)
)

fun stripAllTags(inputText: String): String = stripAllInstances(
    inputText = inputText,
    tagRegex = Regex("<[^<>]*>")
)

fun sanitizeText(inputText: String): String{
    val noBreaks = replaceAllInstances(inputText, generateSelfClosingTagRegex("br"), "\n")
    val noScripts = stripAllInstances(
        stripTagName(noBreaks, "script"),
        generateSelfClosingTagRegex("script")
    )
    val noLinks = replaceTagName(noScripts, "a")
    return stripAllTags(noLinks)
}

